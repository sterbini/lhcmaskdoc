# **The main configuration file**

The main configuration file (in the following called ```main.mask```) allows the user to configure the simulation.

!!! hint
    Examples of main configuration files can be found in ```/afs/cern.ch/eng/tracking-tools/modules/examples``` and [on github](https://github.com/lhcopt/lhcmask/blob/master/examples){target=_blank}. 

The ```main.mask``` consists of different sections:

  - [Preparation of the environment](#preparation-of-the-environment)
  - [Configuration section](#configuration-section) including:
      - [Choice of the make sequence script](#choice-of-the-make-sequence-script)
      - [Choice of the optics](#choice-of-the-optics)
      - [Beam and machine parameters](#beam-and-machine-parameters)
      - [Beam-beam modeling](#Parameters-for-the-beam-beam-modules)
      - [Levelling in IP8](#leveling-in-ip8)
  - [Magnetic and alignment errors and correction](#magnetic-and-alignment-errors-and-correction)
  - [Run the madx scripts and mask modules](#run-the-madx-scripts-and-mask-modules)


## **Preparation of the environment**

In the following we will refer the example you can find [here](https://github.com/lhcopt/lhcmask/blob/master/examples/hl_lhc_collision/main.mask){target=_blank}.

In this section the user defines the paths for the tools that will be used in the following.

```fortran
! Check documentation at http://lhcmaskdoc.web.cern.ch/mainmask/
system, "ln -fns /afs/cern.ch/eng/tracking-tools tracking_tools";

! Mask code folder
system, "ln -fns tracking_tools/modules modules";

! Machine folder
system, "ln -fns tracking_tools/machines machines";

! Toolkit folder
system, "ln -fns tracking_tools/tools tools";

! Beam-beam macros folder
system, "ln -fns tracking_tools/beambeam_macros beambeam_macros";

! Errors folder
system, "ln -fns tracking_tools/errors errors";
```

Please not that we are pointing to the ```tracking_tools``` repository and from them we link the other folders. In particular 

  - the mask modules,
  - the machine folder (from which the make_sequence script can be taken),
  - the tools (for optics independent macros),
  - the beam-beam macros,
  - the errors macros.

!!! hint
    To know more about the *-fns* option check [here](https://superuser.com/questions/81164/why-create-a-link-like-this-ln-nsf){target=_blank}.

Each folder of the previous is associate to a github repository linked to https://github.com/lhcopt:

  - the [mask modules repository](https://github.com/lhcopt/lhcmask){target=_blank},
  - the [machine folder repository](https://github.com/lhcopt/lhcmachines){target=_blank},
  - the [tools repository](https://github.com/lhcopt/lhctoolkit){target=_blank},
  - the [beam-beam macros repository](https://github.com/lhcopt/beambeam_macros){target=_blank},
  - the [errors macros repository](https://github.com/lhcopt/lhcerrors){target=_blank}.

Updated versions folders will be maintained in the AFS optics repository at:

 - ```/afs/cern.ch/eng/tracking-tools```


## **Configuration section**

### **Choice of the make sequence script**


The user needs to provide the path to a MAD-X script that build the machine sequences and link it to ```make_sequence.madx```:
```fortran
! Choose build machine script (sequence, makethin, optics, optics toolkit, cycle)
system, "ln -fns machines/sequences/hl14_thin.madx make_sequence.madx";
```

You can find an example [here](https://github.com/lhcopt/lhcmachines/blob/master/sequences/hl14_thin.madx){target=_blank}.

The script 

 - specify variables for the machine version (```ver_lhc_run```, ```ver_hllhc_optics```)
 - specify the link to the optics repository folders and calls the ```macro.madx``` from the **optics dependent** toolkit.
 - call the sequence(s)
 - edit the sequence(s), if needed
 - slice the sequence, if needed
 - install placeholders for the errors
 - rotate the sequence(s) to make it start from IP3

!!! warning
    Some of the modules assume that the sequences starts from IP3 then the script should cycle the machine to start from IP3.

!!! hint
    Predefined machine scripts for common simulation scenarios are available in the machine folder defined above and on [github](https://github.com/lhcopt/lhcmachines){target=_blank}.

### **Choice of the optics**

The user needs to provide the path to a madx script defining the optics and link it to optics.madx:
```fortran
! Choose optics (magnet strengths)
system, "ln -fns machines/optics/hl14_collision.madx optics.madx";
```

### **Beam and machine parameters**

In this section the users define the settings of the simulations.

Be setting the following flag
```fortran
par_verbose              = 1;
```
the users enable (1) or disable (0) the verbose output.

The sequence to be tracked is selected by:
```fortran
mylhcbeam                = 1;            ! LHC beam 1 (clockwise), LHC beam 2 (clockwise), LHC beam 2 (counterclockwise)
```
with the usual convention:  
1 --> LHC beam 1 (clockwise)  
2 --> LHC beam 2 (clockwise)  
4 --> LHC beam 2 (counterclockwise)

!!! info
    The sequence for the LHC beam 1 counterclockwise (MAD-X beam 3) is not available.

The bunch parameters are specified by:
```fortran
! Beam parameters
par_beam_norm_emit       = %EMIT_BEAM;   ! [um]
par_beam_sigt            = 0.076;        ! [m]
par_beam_sige            = 1.1e-4;       ! [-]
par_beam_npart           = %NPART;       ! [-]
par_beam_energy_tot      = 7000;         ! [GeV]
```

Chromaticity, octupoles and RF voltage are specified by:
```fortran
par_oct_current          = -235;         ! [A]
par_chromaticity         = 5;            ! [-] (Q'=5 for colliding bunches, Q'=15 for non-colliding bunches)
par_vrf_total            = 16.;          ! [MV]
```

Tunes are defined by:
```fortran
! Tunes
par_qx0                  = 62.31;
par_qy0                  = 60.32;
```

Orbit knobs (e.g., crossing angles and separations at the IPs) are defined by:
```fortran
!IP specific orbit settings
par_x1                   = %XING;        ! [urad]
par_sep1                 = 0;            ! [mm]
par_x2                   = -170;         ! [urad]
par_sep2                 = 0.138;        ! [mm]
par_x5                   = par_x1;       ! [urad]
par_sep5                 = 0;            ! [mm]
par_x8                   = -250;         ! [urad]
par_sep8                 = -0.043;       ! [mm]
par_a1                   = 0;            ! [urad]
par_o1                   = 0;            ! [mm]
par_a2                   = 0;            ! [urad]
par_o2                   = 0;            ! [mm]
par_a5                   = 0;            ! [urad]
par_o5                   = 0;            ! [mm]
par_a8                   = 0;            ! [urad]
par_o8                   = 0;            ! [mm]
par_crab1                = -190;         ! [urad]
par_crab5                = par_crab1;    ! [urad]     
```

The phase advance between IP1 and IP5 can be matched by defining the variable (leaving it at zero disables the matching):
```fortran
! Phase advance
par_mux_ip15             = 0;
par_muy_ip15             = 0;     
```

The dispersion correction knob is set by:
```fortran
! Dispersion correction knob
par_on_disp              = 1;
```

The magnets of the experiments are set by:
```fortran
par_on_alice             = 1;
par_on_lhcb              = 1;

par_on_sol_atlas         = 0;
par_on_sol_cms           = 0;
par_on_sol_alice         = 0;
```

### **Parameters for the beam beam modules**

The following flag informs the code that head-on collisions are present:
```fortran
par_on_collision         = 1;            ! If 1 lumi leveling in ip8 is applied and q/q' match is done with bb off 
```

NB. This has no effect on the orbit in IP1/5 (the separations are set by the knobs defined above). The only effects of this flag are:
- Enables the luminosity leveling in IP8
- Tunes and chromaticity are matched with bb if ```par_on_collision = 0```, without beam-beam if ```par_on_collision = 1```

The installation of beam-beam lenses is enabled by:
```fortran
par_on_bb_switch = 1;
```

The bunch spacing is defined by:
```fortran
par_b_t_dist             = 25.;          ! bunch separation [ns]
```
It will be then adjusted to match an integer number of RF buckets

Long-range encountered are installed by default up to D1. The number of additional encounters in the D1 is defined by:
```fortran
par_n_inside_D1          = 5;            ! default value for the number of additional parasitic encounters inside D1
```

One lens installed for each head-on slice. The number of head-on slices:

```fortran
par_nho_IR1              = 11;           ! number of slices for head-on in IR1 (between 0 and 201)
par_nho_IR2              = 11;           ! number of slices for head-on in IR2 (between 0 and 201)
par_nho_IR5              = 11;           ! number of slices for head-on in IR5 (between 0 and 201)
par_nho_IR8              = 11;           ! number of slices for head-on in IR8 (between 0 and 201)
```

The installation of the crab-cavities is enabled by:
```fortran
par_install_crabcavities = 1;
```
This is now done by the beam-beam macros. It will be modified in the near future.

### **Leveling in IP8**

The target luminosity in IP8 is specified by:
```fortran
! This variables set the leveled luminosity in IP8 (considered if par_on_collision=1)
par_lumi_ip8             = 2e33;         ![Hz/cm2]
```

The number of colliding bunches for all IPs is specified by:
```
! These variables define the number of Head-On collisions in the 4 IPs
par_nco_IP1              = 2748;
par_nco_IP2              = 2494;
par_nco_IP5              = par_nco_IP1;
par_nco_IP8              = 2572;
```
NB. Luminosity leveling in IP8 is applied only if ```par_on_collision =1```


### **Magnetic and alignment errors and correction**

Magnetic and alignment errors and correction are defined by:
```fortran
! Select seed for errors
par_myseed               = %SEEDRAN;

! Set this flag to correct the errors of D2 in the NLC (warning: for now only correcting b3 of D2, still in development)
par_correct_for_D2       = 0;
! Set this flag to correct the errors of MCBXF in the NLC (warning: this might be less reproducable in reality, use with care)
par_correct_for_MCBX     = 0;

par_on_errors_LHC        = 1;
par_on_errors_MBH        = 1;
par_on_errors_Q5         = 1;
par_on_errors_Q4         = 1;
par_on_errors_D2         = 1;
par_on_errors_D1         = 1;
par_on_errors_IT         = 1;
par_on_errors_MCBRD      = 0;
par_on_errors_MCBXF      = 0;
```

## **Run the madx scripts and mask modules**

The last part of the ```main.mask``` will actually do the job, by calling the previously linked madx files to build the sequence and setup the optics:

```fortran
! (the user can introduce in this section additional custom code 
! to edit machine and/or optics)
call, file="make_sequence.madx";
call, file="optics.madx";  
```

and by calling the mask modules (the functionality of each module is described in [another section of this site](modules.md)):
```fortran
call, file="modules/module_01_orbit.madx";
call, file="modules/module_02_lumilevel.madx";
call, file="modules/module_03_beambeam.madx";
call, file="modules/module_04_errors.madx";
call, file="modules/module_05_tuning.madx";
call, file="modules/module_06_generate.madx";
```

Selected modules (e.g., the bb or the errors) can be disabled by removing the corresponding call. In that case please remember to setup the corresponding flags (e.g. par_on_bb_switch).

!!! warning
    The order of the modules has to be respected.
