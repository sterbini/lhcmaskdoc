## **Structure of the main configuration file**

### Importing the relevant packages
After having imported some standard packages
```python
import sys, os
import pickle
import numpy as np
```
the user will import the *pymask* package (coming with the [*lhcmask*](https://github.com/lhcopt/lhcmask){target=_blank} repository) 
```python
# Import pymask
sys.path.append('../../')
import pymask as pm
import pymask.luminosity as lumi
Madx = pm.Madxp
```
The *pymask* package contains the *Madxp* module (a [*cpymad*](https://github.com/hibtc/cpymad){target=_blank} wrapper) 
and other useful modules, like the *luminosity* one.

Then the user imports two user-defined modules in the working folder called

- [optics_specific_tools](https://github.com/lhcopt/lhcmask/blob/master/python_examples/hl_lhc_collisions_python/optics_specific_tools.py){target=_blank}, 
it contains optics specific functions (e.g., to makethin the sequence, to load the optics, to make checks,...).

- [mask_parameters](https://github.com/lhcopt/lhcmask/blob/master/python_examples/hl_lhc_collisions_python/mask_parameters.py){target=_blank}: 
it contains the parameters used in the scripts.


!!! info
    There is another important input file called [knob_parameters](https://github.com/lhcopt/lhcmask/blob/master/python_examples/hl_lhc_collisions_python/knob_parameters.py){target=_blank}
    that contains the knob settings of the sequence.
    Strinctly speaking, those three files could be merged in the present script, 
    but we suggest to keep them separate for improving its readibility/modularity.

```
import optics_specific_tools as ost
from mask_parameters import mask_parameters
```

### Selecting the beam mode

The user needs then to select the beam mode. There are 
6 beam modes presently available:

- b1_without_bb: track b1 without beam-beam
- b1_with_bb: track b1 with beam-beam
- b4_from_b2_without_bb: track b4 building it from b2 without beam-beam
- b4_from_b2_with_bb: track b4 building it from b2 with beam-beam
- b1_with_bb_legacy_macros (legacy mode): track b1 with beam-beam using the legacy macros
- b4_without_bb (legacy mode): track b4 using the b4 sequence and therefore 
without beam-beam. In fact the b4 sequence does not contain the strong beam
(b3), therefore we cannot compute the beam-beam simply using the b4 sequence.


A beam mode will set several flags. We use a "beam mode" variable instead to set 
each single flags to improve the user's experience and to minimize the risk of
inconsistent settings. 

```python

###############
# Select mode #
###############

#mode = 'b1_without_bb'
#mode = 'b1_with_bb'
#mode = 'b4_from_b2_without_bb'
mode = 'b4_from_b2_with_bb'

# Legacy modes
#mode = 'b1_with_bb_legacy_macros'
#mode = 'b4_without_bb'

```

!!! info
	We would like to make a digression on b4. For **periodic** linear problems
	we can twiss the machine optics for either b2 or b4 and find an equivalent
	periodic solutions.
	The tracking problem is different. Let's assume a kicker introducing a betatronic 
	oscillation: clearly the direction of the beam is important to describe the
	beam trajectory (i.e., to respect of the causality principle).
	
	In the optics repository we have b1, b2 and b4 sequences but we miss the b3 one.
	For the  beam-beam (BB) simulations we needs both beams, that is b1+b2 or b3+b4. 
	Since we do lack b3, in order
	to implement BB kicks in b4, we compute the BB in the b1+b2 configuration. We compute the BB lenses
	in b2, then, starting from the b2 elements, we populate the attributes of the 
	b4 sequence (we call it the b4_from_b2 "transplant").

The flexibility in python allows us to make checks all along the script in a more 
systematic way. The user should introduce them for monitoring
the sanity of the code.
As we will show, there are optics independent functions that ease the sanity 
assertions.

To accomplish this checks, the user needs to define some tolerances, e.g., 
IP dependent tolerances for the
beta-function or the beam separations.


```python
########################
# Other configurations #
########################

# Tolarances for checks [ip1, ip2, ip5, ip8]
tol_beta = [1e-3, 10e-2, 1e-3, 1e-2]
tol_sep = [1e-6, 1e-6, 1e-6, 1e-6]
```

### Defining the links
To define the links to the repository folders one can use function 
[**make_links**](https://github.com/lhcopt/lhcmask/blob/master/pymask/pymasktools.py#L8){target=_blank}.
As mentioned, all links should point to /afs/cern.ch/eng/tracking-tools 
(or to the clones of the relevant git repository).

!!! Hint
	We encourage to use the *afs* central version of the MAD-X modules and macros 
	to ease their maintenance and to ensure the natural propagation of the fixes. 
	The user should customize its own **000_pymask.py** script and the optics-dependent modules but 
	not the optics independent ones. Clearly the user is welcome to
	contribute by forking, editing and pulling a request in the git repository.
	That said, during the developing/test/check of the mask, it is convenient to
	use local repositories. To this end, we suggest to keep (as an example) a *customization.bash* 
	script to your working folder (not tracked in the git repository) with the 
	function to overwrite the official repository links with local/customizable ones.
	These should be used only during the setup of the simulations and, enventually, 
	it will be merged to the official repository than will be pulled to the *afs* repository.
	The *afs* repository will be used for the productions studies by removing the *customization.bash* file.

```python
pm.make_links(force=True, links_dict={
    'tracking_tools': '/afs/cern.ch/eng/tracking-tools',
    'modules': 'tracking_tools/modules',
    'tools': 'tracking_tools/tools',
    'beambeam_macros': 'tracking_tools/beambeam_macros',
    'errors': 'tracking_tools/errors'})

# Execute customization script if present
os.system('bash customization.bash')
```

As mentioned, the user is the owner and responsible of the input script and can/should customize it to
improve the workflow. For example she/he can add variables. 

```python
# Choose optics file
optics_file = '/afs/cern.ch/eng/lhc/optics/HLLHCV1.4/round/opt_round_150_1500_thin.madx' #15 cm
check_betas_at_ips = True
check_separations_at_ips = True
# to save in parquet files the intermediate twiss
# for further checks or plots 
save_intermediate_twiss = True
```

### Check and load the parameters.

The [**checks_on_parameter_dict**](https://github.com/lhcopt/lhcmask/blob/feed52aca2a07aa1c7e7cffd9ba69de952e2d724/pymask/pymasktools.py#L157){target=_blank}
parameter functions will check and load the
script parameters.

!!! Hint
	Please use the python namespace to associate the fuction to a specific
	package/modules. This will help you to find the function definition.
	
```python
######################################
# Check parameters and activate mode #
######################################

# Check and load parameters 
pm.checks_on_parameter_dict(mask_parameters)
```

### Configuration definition
Depending on the beam mode, different flags will be set accordingly.
In fact, as already mentioned, a beam mode is nothing else that a consistent 
set of flags. Check here the code of [**get_pymask_configuration**](https://github.com/lhcopt/lhcmask/blob/master/pymask/pymasktools.py#L15){target=_blank}

```python
# Define configuration
(beam_to_configure, sequences_to_check, sequence_to_track, generate_b4_from_b2,
    track_from_b4_mad_instance, enable_bb_python, enable_bb_legacy,
    force_disable_check_separations_at_ips,
    ) = pm.get_pymask_configuration(mode)

if force_disable_check_separations_at_ips:
    check_separations_at_ips = False

if not(enable_bb_legacy) and not(enable_bb_python):
    mask_parameters['par_on_bb_switch'] = 0.

```

### Starting MAD-X
And finally you can start MAD-X

```python
########################
# Build MAD-X instance #
########################

# Start mad
mad = Madx()
```

###  Build the sequence
This is the first example of function that the user needs to define since it is 
optics dependent. Tipically the user wants to modify the *as_built* sequence by
adding special devices, markers, by rotating it or making it thin. All these
manipulations can be done there. You can check [here](https://github.com/lhcopt/lhcmask/blob/feed52aca2a07aa1c7e7cffd9ba69de952e2d724/python_examples/hl_lhc_collisions_python/optics_specific_tools.py#L6){target=_blank}
an example.

```python
# Build sequence
ost.build_sequence(mad, beam=beam_to_configure)
```

### Load a specific optics
This is the second example of user's defined function, to call the
stregth file. As an example, you can check [here](https://github.com/lhcopt/lhcmask/blob/feed52aca2a07aa1c7e7cffd9ba69de952e2d724/python_examples/hl_lhc_collisions_python/optics_specific_tools.py#L10){target=_blank}.

```python
# Apply optics
ost.apply_optics(mad, optics_file=optics_file)
```

### Load parameters to mad
You can load the parameters to mad with [set_variables_from_dict](https://github.com/lhcopt/lhcmask/blob/master/pymask/madxp.py#L106){target=_blank}

```python
# Pass parameters to mad
mad.set_variables_from_dict(params=mask_parameters)
```

### Prepare the sequences and attach the beams
Now we start to call the MAD-X modules (these are MAD-X script.). They are optics independent.

!!! warning
    The update documentations of the modules and submodules is not yet available.
	Having a link "module" in the working folder allow us to inspect the code in
	the  modules. 

!!! info
    Check the code here:
    
    - [submodule_01a_preparation.madx](https://github.com/lhcopt/lhcmask/blob/master/submodule_01a_preparation.madx){target=_blank}
    - [submodule_01b_beam.madx](https://github.com/lhcopt/lhcmask/blob/master/submodule_01b_beam.madx){target=_blank}


```python
# Prepare auxiliary mad variables
mad.call("modules/submodule_01a_preparation.madx")

# Attach beams to sequences
mad.call("modules/submodule_01b_beam.madx")
```


### Check the machine of the repository
It is important to note that the user's knob are not yet applied. 
Therefore the sequence(s) has(have) the knobs status as set from the optics repository.

!!! hint
    The test of the status of the machine along the execution of the script 
    should be systematically checked by the user. As an example, the user can 
    define a optics dependent function [**twiss_and_check**](https://github.com/lhcopt/lhcmask/blob/feed52aca2a07aa1c7e7cffd9ba69de952e2d724/python_examples/hl_lhc_collisions_python/optics_specific_tools.py#L92){target=_blank}
    allowing to save the twiss files of the sequence(s) considered.

!!! question
	TODO: Why do we put this function in the **optics_specific_tools**? 
	
```python
# Test machine before any change
twiss_dfs, other_data = ost.twiss_and_check(mad, sequences_to_check,
        tol_beta=tol_beta, tol_sep=tol_sep,
        twiss_fname='twiss_from_optics',
        save_twiss_files=save_intermediate_twiss,
        check_betas_at_ips=check_betas_at_ips,
        check_separations_at_ips=check_separations_at_ips)
```

### Set IP1/5 phase, apply and save crossing
!!! info
    Check the code here:
    
    - [submodule_01c_phase.madx](https://github.com/lhcopt/lhcmask/blob/master/submodule_01c_phase.madx){target=_blank}

```python
# Set IP1-IP5 phase and store corresponding reference
mad.call("modules/submodule_01c_phase.madx")
```
### Set optics-specific knobs
Here the user needs to take care of the different conventions for the knob
definitions: some of the knobs definition are optics dependent (unfortunately). 
The user needs to make the proper knob conversion defining the function
[**set_optics_specific_knobs**](https://github.com/lhcopt/lhcmask/blob/feed52aca2a07aa1c7e7cffd9ba69de952e2d724/python_examples/hl_lhc_collisions_python/optics_specific_tools.py#L27){target=_blank}

```python
# Set optics-specific knobs
ost.set_optics_specific_knobs(mad, mode)
```

### Crossing-save and some reference measurements
At this point of the script the machine is configured with the user knobs and 
the **crossing_save** macro is executed.

!!! info
    Check the code here:
    
    - [submodule_01e_final.madx](https://github.com/lhcopt/lhcmask/blob/master/submodule_01e_final.madx){target=_blank}

```python
# Crossing-save and some reference measurements
mad.input('exec, crossing_save')
mad.call("modules/submodule_01e_final.madx")
```

### Test flat machine
It is a good habit to check the status of the machine with flat orbit and 
introduce some automatic assertions as sanity check.

!!! info
    Check the code here:
    
    - [twiss_and_check](https://github.com/lhcopt/lhcmask/blob/master/python_examples/hl_lhc_collisions_python/optics_specific_tools.py#L92){target=_blank}

```python
#################################
# Check bahavior of orbit knobs #
#################################

# Check flat machine
mad.input('exec, crossing_disable')
twiss_dfs, other_data = ost.twiss_and_check(mad, sequences_to_check,
        tol_beta=tol_beta, tol_sep=tol_sep,
        twiss_fname='twiss_no_crossing',
        save_twiss_files=save_intermediate_twiss,
        check_betas_at_ips=check_betas_at_ips, check_separations_at_ips=check_separations_at_ips)

# Check orbit flatness
flat_tol = 1e-6
for ss in twiss_dfs.keys():
    tt = twiss_dfs[ss]
    assert np.max(np.abs(tt.x)) < flat_tol
    assert np.max(np.abs(tt.y)) < flat_tol
```

### Check machine after crossing restore

!!! info
    Check the code here:
    
    - [twiss_and_check](https://github.com/lhcopt/lhcmask/blob/master/python_examples/hl_lhc_collisions_python/optics_specific_tools.py#L92){target=_blank}

```python
# Check machine after crossing restore
mad.input('exec, crossing_restore')
twiss_dfs, other_data = ost.twiss_and_check(mad, sequences_to_check,
        tol_beta=tol_beta, tol_sep=tol_sep,
        twiss_fname='twiss_with_crossing',
        save_twiss_files=save_intermediate_twiss,
        check_betas_at_ips=check_betas_at_ips, check_separations_at_ips=check_separations_at_ips)

```

### Luminosity leveling

```python
#################################
# Set luminosity in IP2 and IP8 #
#################################

# Luminosity levelling
print('Luminosities before leveling (crab cavities are not considered):')
lumi.print_luminosity(mad, twiss_dfs,
        mask_parameters['par_nco_IP1'], mask_parameters['par_nco_IP2'],
        mask_parameters['par_nco_IP5'], mask_parameters['par_nco_IP8'])

if enable_bb_legacy:
    mad.use(f'lhcb{beam_to_configure}')
    if mode=='b4_without_bb':
        print('Leveling not working in this mode!')
    else:
        mad.call("modules/module_02_lumilevel.madx")
else:
    from scipy.optimize import least_squares

    # Leveling in IP8
    L_target_ip8 = mask_parameters['par_lumi_ip8']
    def function_to_minimize_ip8(sep8v_m):
        my_dict_IP8=lumi.get_luminosity_dict(
            mad, twiss_dfs, 'ip8', mask_parameters['par_nco_IP8'])
        my_dict_IP8['y_1']=np.abs(sep8v_m)
        my_dict_IP8['y_2']=-np.abs(sep8v_m)
        return np.abs(lumi.L(**my_dict_IP8) - L_target_ip8)
    sigma_x_b1_ip8=np.sqrt(twiss_dfs['lhcb1'].loc['ip8:1'].betx*mad.sequence.lhcb1.beam.ex)
    optres_ip8=least_squares(function_to_minimize_ip8, sigma_x_b1_ip8)
    mad.globals['on_sep8'] = np.sign(mad.globals['on_sep8']) * np.abs(optres_ip8['x'][0])*1e3

    # Halo collision in IP2
    sigma_y_b1_ip2=np.sqrt(twiss_dfs['lhcb1'].loc['ip2:1'].bety*mad.sequence.lhcb1.beam.ey)
    mad.globals['on_sep2']=np.sign(mad.globals['on_sep2'])*mask_parameters['par_fullsep_in_sigmas_ip2']*sigma_y_b1_ip2/2*1e3

    # Re-save knobs
    mad.input('exec, crossing_save')

# Check machine after leveling
mad.input('exec, crossing_restore')
twiss_dfs, other_data = ost.twiss_and_check(mad, sequences_to_check,
        tol_beta=tol_beta, tol_sep=tol_sep,
        twiss_fname='twiss_after_leveling',
        save_twiss_files=save_intermediate_twiss,
        check_betas_at_ips=check_betas_at_ips, check_separations_at_ips=check_separations_at_ips)

print('Luminosities after leveling (crab cavities are not considered):')
lumi.print_luminosity(mad, twiss_dfs,
        mask_parameters['par_nco_IP1'], mask_parameters['par_nco_IP2'],
        mask_parameters['par_nco_IP5'], mask_parameters['par_nco_IP8'])

```

#### Switch off the dispersion correction

```python
#####################
# Force on_disp = 0 #
#####################

mad.globals.on_disp = 0.
# will be restored later
```

### Compute the beam-beam configuration

!!! info
    Check the code here:
    
    - [generate_bb_dataframes](https://github.com/lhcopt/lhcmask/blob/master/pymask/beambeam.py#L730){target=_blank}

```python
###################################
# Compute beam-beam configuration #
###################################

# Prepare bb dataframes
if enable_bb_python:
    bb_dfs = pm.generate_bb_dataframes(mad,
        ip_names=['ip1', 'ip2', 'ip5', 'ip8'],
        harmonic_number=35640,
        numberOfLRPerIRSide=[25, 20, 25, 20],
        bunch_spacing_buckets=10,
        numberOfHOSlices=11,
        bunch_population_ppb=None,
        sigmaz_m=None,
        z_crab_twiss = 0.075,
        remove_dummy_lenses=True)

    # Here the datafremes can be edited, e.g. to set bbb intensity

```

### Generate the b4

!!! info
    Check the code here:
    
    - [twiss_and_check](https://github.com/lhcopt/lhcmask/blob/master/python_examples/hl_lhc_collisions_python/optics_specific_tools.py#L92){target=_blank}



```python
###################
# Generate beam 4 #
###################

if generate_b4_from_b2:
    mad_b4 = Madx()
    ost.build_sequence(mad_b4, beam=4)
    ost.apply_optics(mad_b4, optics_file=optics_file)

    pm.configure_b4_from_b2(mad_b4, mad)

    twiss_dfs_b2, other_data_b2 = ost.twiss_and_check(mad,
            sequences_to_check=['lhcb2'],
            tol_beta=tol_beta, tol_sep=tol_sep,
            twiss_fname='twiss_b2_for_b4check',
            save_twiss_files=save_intermediate_twiss,
            check_betas_at_ips=check_betas_at_ips, check_separations_at_ips=False)

    twiss_dfs_b4, other_data_b4 = ost.twiss_and_check(mad_b4,
            sequences_to_check=['lhcb2'],
            tol_beta=tol_beta, tol_sep=tol_sep,
            twiss_fname='twiss_b4_for_b4check',
            save_twiss_files=save_intermediate_twiss,
            check_betas_at_ips=check_betas_at_ips, check_separations_at_ips=False)

```

### Select the beam to track

```python
##################################################
# Select mad instance for tracking configuration #
##################################################

# We working exclusively on the sequence to track
# Select mad object
if track_from_b4_mad_instance:
    mad_track = mad_b4
else:
    mad_track = mad

mad_collider = mad
del(mad)

# Twiss machine to track
twiss_dfs, other_data = ost.twiss_and_check(mad_track, sequences_to_check,
        tol_beta=tol_beta, tol_sep=tol_sep,
        twiss_fname='twiss_track_intermediate',
        save_twiss_files=save_intermediate_twiss,
        check_betas_at_ips=check_betas_at_ips, check_separations_at_ips=False)

```

### Install the BB lenses

```python
#####################
# Install bb lenses #
#####################

# Python approach
if enable_bb_python:
    if track_from_b4_mad_instance:
        bb_df_track = bb_dfs['b4']
        assert(sequence_to_track=='lhcb2')
    else:
        bb_df_track = bb_dfs['b1']
        assert(sequence_to_track=='lhcb1')

    pm.install_lenses_in_sequence(mad_track, bb_df_track, sequence_to_track)

    # Disable bb (to be activated later)
    mad_track.globals.on_bb_charge = 0
else:
    bb_df_track = None

# Legacy bb macros
if enable_bb_legacy:
    assert(beam_to_configure == 1)
    assert(not(track_from_b4_mad_instance))
    assert(not(enable_bb_python))
    mad_track.call("modules/module_03_beambeam.madx")

```

### Install the crab cavities

```python
#########################
# Install crab cavities #
#########################

mad_track.call("modules/submodule_04_1a_install_crabs.madx")

# Save references (orbit at IPs)
mad_track.call('modules/submodule_04_1b_save_references.madx')

```

#### Switch off the dispersion correction

```python
#####################
# Force on_disp = 0 #
#####################

mad_track.globals.on_disp = 0.
# will be restored later

```
#### Final use method
```python
#############
# Final use #
#############

mad_track.use(sequence_to_track)
# Disable use
mad_track._use = mad_track.use
mad_track.use = None

```

### Install errors

```python
##############################
# Install and correct errors #
##############################

mad_track.call("modules/module_04_errors.madx")

```

### Machine tuning


```python
###############################
# Machine tuning (enables bb) #
###############################

mad_track.call("modules/module_05_tuning.madx")

# Switch on crab cavities
mad_track.globals.on_crab1 = mad_track.globals.par_crab1
mad_track.globals.on_crab5 = mad_track.globals.par_crab5

```

### Generate SixTrack input

```python
#####################
# Generate sixtrack #
#####################

if enable_bb_legacy:
    mad_track.call("modules/module_06_generate.madx")
else:
    pm.generate_sixtrack_input(mad_track,
        seq_name=sequence_to_track,
        bb_df=bb_df_track,
        output_folder='./',
        reference_bunch_charge_sixtrack_ppb=(
            mad_track.sequence[sequence_to_track].beam.npart),
        emitnx_sixtrack_um=(
            mad_track.sequence[sequence_to_track].beam.exn),
        emitny_sixtrack_um=(
            mad_track.sequence[sequence_to_track].beam.eyn),
        sigz_sixtrack_m=(
            mad_track.sequence[sequence_to_track].beam.sigt),
        sige_sixtrack=(
            mad_track.sequence[sequence_to_track].beam.sige),
        ibeco_sixtrack=1,
        ibtyp_sixtrack=0,
        lhc_sixtrack=2,
        ibbc_sixtrack=0,
        radius_sixtrack_multip_conversion_mad=0.017,
        skip_mad_use=True)

```

#### Save optics and orbit at start ring

```python
#######################################
# Save optics and orbit at start ring #
#######################################

optics_orbit_start_ring = pm.get_optics_and_orbit_at_start_ring(
        mad_track, sequence_to_track, skip_mad_use=True)
with open('./optics_orbit_at_start_ring.pkl', 'wb') as fid:
    pickle.dump(optics_orbit_start_ring, fid)

```

#### Generate pysixtrack lines

```python
#############################
# Generate pysixtrack lines #
#############################

if enable_bb_legacy:
    print('Pysixtrack line is not generated with bb legacy macros')
else:
    pysix_fol_name = "./pysixtrack"
    dct_pysxt = pm.generate_pysixtrack_line_with_bb(mad_track,
        sequence_to_track, bb_df_track,
        closed_orbit_method='from_mad',
        pickle_lines_in_folder=pysix_fol_name,
        skip_mad_use=True)

```

## **Handling of masked parameters (for SixDesk)**