# **Structure and interface**
A working example, can be found in the folder [```python_examples/hl_lhc_collisions_python/```](https://github.com/lhcopt/lhcmask/tree/master/python_examples/hl_lhc_collisions_python){target=_blank}
of the repository (a short guide on how to run the example is available [here](pymask_run_example.md){target=_blank}).

The simulation can be defined by editing the following three files:

 * ```000_pymask.py```: is the main file to be executed, which performs the 
  different steps of the calculations, as defined in the section ["step-by-step description"](pymask_step_by_step.md){target=_blank}.
  For usual simulation scenarios this file does not
  need to be edited, and the configuration can be modified through the files ```config.py``` and 
  ```optics_specific_tools.py```.
 * ```config.py```: containing the main simulation settings and parameters as well as 
   the optics-specific knob names. A detailed description of this file is available [here](config_py_doc.md).
 * ```optics_specific_tools.py```: containing user-defined python functions to build 
   the MAD-X sequences of the collider, load an optics file, configure optics-specific 
   knobs (e.g. crossing angles and separations at the interaction points), perform 
   sanity checks and save customized optics calculation results. This introduces an intermediate layer between the mask and the optics repository, allowing the use of the same python mask with optics that require different manipulations (e.g. to build the sequence) and have different knob naming conventions.

These files make use of the [```pymask``` python package](https://github.com/lhcopt/lhcmask/tree/master/pymask){target=_blank} 
which is available in the repository.

It is assumed that the optics exposes three macros to control the orbit bumps in the machine:

  * ```crossing_save```, which saves the state of all the orbit knobs;
  * ```crossing_disable```, which sets to zero all the orbit knobs (going to "flat" orbit);
  * ```crossing_restore```, which resets the knobs to the saved values.

Through this mechanism the code does not depend on the specific knob names used by the optics. If these macros are not available or they need to be modified, this can be done in the ```build_sequence``` function of the ```optics_specific_tools``` as done in [this example](https://github.com/lhcopt/lhcmask/blob/master/python_examples/run3_collisions_python/optics_specific_tools.py){target=_blank}.

 The only knobs that are used directly by the mask are ```on_disp``` which controls the dispersion correction bump (if present) and ```on_crab1```/```on_crab5``` which control the crab cavities (if present).
