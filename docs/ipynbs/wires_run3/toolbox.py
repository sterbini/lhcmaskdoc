# %%
# ============================================================================================================ #
#                                              WORKFLOW FUNCTIONS                                              #
# ============================================================================================================ #

# %%

# %% Import few packages
from cpymad.madx import Madx
#import madxp
from madxp import cpymadTool as mt
from collections import OrderedDict
import numpy as np
import pandas as pd
from matplotlib import pylab as plt
from IPython.display import display
import time
import os
import warnings
warnings.filterwarnings('always')
from cl2pd import importData
from cl2pd import particle

def make_sequence(mad, mylhcbeam, slicefactor, wires_lst=None):
    '''
    Function to build the machine sequence in MAD-X.
    
    Args: 
        mad: madx instance
        mylhcbeam: int, considered beam 
        slicefactor: int, determines in how many slices each element of the sequence will be sliced
        wires_lst: lst or None, if you want to install wires in the sequence, provide the list of 
                    the wanted wires (WireObj instances)
    '''
    start_time = time.time()
    mad.input('option, -echo,warn,-info;')
    mad.call('/afs/cern.ch/user/s/sterbini/public/tracking_tools/tools/macro.madx') 
    mad.call('tools/optics_indep_macros.madx')

    assert mylhcbeam in [1, 2, 4], "Invalid mylhcbeam (it should be in [1, 2, 4])"

    if mylhcbeam in [1, 2]:
        mad.call('/afs/cern.ch/eng/lhc/optics/runII/2018/lhc_as-built.seq')
    else:
        mad.call('/afs/cern.ch/eng/lhc/optics/runII/2018/lhcb4_as-built.seq')

    # New IR7 MQW layout and cabling
    mad.call('/afs/cern.ch/eng/lhc/optics/runIII/RunIII_dev/IR7-Run3seqedit.madx')
    
    # Makethin
    if slicefactor > 0:
        mad.input(f'slicefactor={slicefactor};') # the variable in the macro is slicefactor
        mad.call('/afs/cern.ch/eng/lhc/optics/runII/2018/toolkit/myslice.madx')
        mad.beam()
        for my_sequence in ['lhcb1','lhcb2']:
            if my_sequence in list(mad.sequence):
                mad.input(f'use, sequence={my_sequence}; makethin, sequence={my_sequence}, style=teapot, makedipedge=false;')
    else:
        warnings.warn('The sequences are not thin!')

    # Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
    for my_sequence in ['lhcb1','lhcb2']:
        if my_sequence in list(mad.sequence):
            mad.input(f'seqedit, sequence={my_sequence}; flatten; cycle, start=IP3; flatten; endedit;')
    
    # Install wires
    if wires_lst:
        for wire in wires_lst:
            wire.define_in_mad()
            wire.install_in_sequence()

    my_output_dict = get_status(mad)
    elapsed_time = time.time() - start_time
    my_output_dict['elapsed_time'] = elapsed_time
    return my_output_dict

def load_optics(mad, optics_number):
    start_time = time.time()
    mad.call(f'/afs/cern.ch/eng/lhc/optics/runIII/RunIII_dev/2021_V1/PROTON/opticsfile.{int(optics_number)}')
    my_output_dict = get_status(mad)
    elapsed_time = time.time() - start_time

    my_output_dict['elapsed_time'] = elapsed_time
    return my_output_dict

def get_status(mad):
    start_time = time.time()
    variables=mt.variables_dict(mad)
    my_output_dict= {'constant_df': variables['constant_df'],
            'independent_variable_df': variables['independent_variable_df'],
            'dependent_variable_df': variables['dependent_variable_df'],
            'sequences_df': mt.sequences_df(mad),
            'beams_df': mt.beams_df(mad),
            'tables_list': list(mad.table)}
    elapsed_time = time.time() - start_time
    my_output_dict['elapsed_time'] = elapsed_time
    return my_output_dict

def run_module(mad, module_name):
    start_time = time.time()
    mad.call(f'modules/{module_name}')
    elapsed_time = time.time() - start_time
    my_output_dict = get_status(mad)
    elapsed_time = time.time() - start_time
    my_output_dict['elapsed_time']= elapsed_time
    return my_output_dict

def read_parameters(mad, parameter_dict):
    start_time = time.time()
    parameter_dict['par_qx00']=int(parameter_dict['par_qx0'])
    parameter_dict['par_qy00']=int(parameter_dict['par_qy0'])
    parameter_dict['par_tsplit']=parameter_dict['par_qx00']-parameter_dict['par_qy00']
    assert parameter_dict['par_nco_IP5']==parameter_dict['par_nco_IP1']
    assert parameter_dict['par_qx00']-parameter_dict['par_qy00']==parameter_dict['par_tsplit']
    assert 'par_mylhcbeam' in parameter_dict
    assert 'par_beam_norm_emit' in parameter_dict
    for i in parameter_dict:
        mad.input(f'{i}={parameter_dict[i]};')
    my_output_dict = get_status(mad)
    elapsed_time = time.time() - start_time
    my_output_dict['elapsed_time']= elapsed_time
    return my_output_dict

def cleanall(file_string='fc.* *parquet twiss* log.madx stdout.madx bb_lenses.dat last_twiss.0.gz temp', rm_links=True):
    if rm_links:
        os.system('find -type l -delete')
    os.system('rm -rf '+ file_string)
    
def power_wires(wires_lst,table_out):
    '''
    Function to turn on the wires as done in the LHC Run 3.
    
    Args:
        mad: madx instance
        wires_lst: list, contains the wires under the form of WireObj instances
        table_out: str, name of the madx table to be added to "twiss_" and "summary_" 
    Return: 
        my_output_dict: dict, contains the output info to be added to the working flow DF.
    '''
    start_time = time.time()
    
    mad = wires_lst[0].mad_inst
    
    # Turn off BB
    mad.input(f'''
    ON_BB_CHARGE=0;
    ''')

    for wire in wires_lst:
        wire.switch_wire(on=True)
        wire.print_lense()

    mad.input(f'''
    use, sequence={wires_lst[0].sequence};
    twiss, table=twiss_{table_out};
    exec, clonesumm('summary_{table_out}');
    ON_BB_CHARGE=1;
    ''')
    
    my_output_dict = get_status(mad)
    elapsed_time = time.time() - start_time

    my_output_dict['elapsed_time'] = elapsed_time
    return my_output_dict


def make_links():
    #%% Make links for setting the enviroments
    cleanall()
    # Main path
    os.system('ln -fns /afs/cern.ch/eng/tracking-tools tracking_tools')
    # Mask code folder
    os.system('ln -fns /afs/cern.ch/user/s/sterbini/public/tracking_tools/modules modules')
    # Machine folder
    os.system('ln -fns tracking_tools/machines machines')
    # Toolkit folder
    os.system('ln -fns tracking_tools/tools tools')
    # Beam-beam macros folder
    os.system('ln -fns tracking_tools/beambeam_macros beambeam_macros')
    # Errors folder
    os.system('ln -fns tracking_tools/errors errors')
    

def from_beta_to_xing_angle_urad(beta_m):
    return  0.5*(132.47 + 58.3959 * np.sqrt(beta_m) + 30.0211 * beta_m)/np.sqrt(beta_m)


def get_param_from_table(mad_inst, table, param, element):
    index = np.where(mad_inst.table[table].name == element)[0][0]
    return mad_inst.table[table][param][index]
    
#==========================================================================================#
#                                        WIRE EFFECT                                       # 
#==========================================================================================#

# ORBIT EFFECT
def orbit_shift(I_L, r_w, betx, bety, qx, qy, phi_w, Brho, mu0=1.2566370614359173e-06):
    '''
    This functions computes the orbit shift induced by a wire in both planes.
    Taken from https://indico.cern.ch/event/456856/contributions/1968793/attachments/1196177/1740660/BBLR_LYON_2015.pdf
    Args:
        I_L: float or array, current in the wire [A.m]
        r_w: float or array, beam-wire distance, regardless the plane [m]
        phi_w: float or array, angle between the beam and the wires, zero being horizontal [rad]
        Brho: float, magnetic rigidity
        mu0: vacuum permeability [H/m] 
    
    Returns:
        [dx,dy]: lst of dim 2 (for x and y). Each element can be either a float or an array depending on the input.
    '''
    theta_x = mu0*I_L*np.cos(phi_w)/2/np.pi/Brho/r_w
    theta_y = mu0*I_L*np.sin(phi_w)/2/np.pi/Brho/r_w
    dx = betx*theta_x/2/np.sin(np.pi*qx)*np.cos(-np.pi*qx)
    dy = bety*theta_y/2/np.sin(np.pi*qy)*np.cos(-np.pi*qy)
    return [dx,dy]

# TUNE EFFECT
def tune_shift(I_L, r_w, betx, bety, phi_w, Brho, mu0=1.2566370614359173e-06):
    '''
    This functions computes the tune shift induced by a wire in both planes.
    Taken from https://indico.cern.ch/event/456856/contributions/1968793/attachments/1196177/1740660/BBLR_LYON_2015.pdf
    Args:
        I_L: float or array, current in the wire [A.m]
        r_w: float or array, beam-wire distance, regardless the plane [m]
        phi_w: float or array, angle between the beam and the wires, zero being horizontal [rad]
        betx,bety: floats, beta functions at the wire location
        Brho: float, magnetic rigidity
        mu0: vacuum permeability [H/m] 
    
    Returns:
        [dqx,dqy]: lst of dim 2 (for x and y). Each element can be either a float or an array depending on the input.
    '''
    dqx = -mu0*I_L*betx*np.cos(2*phi_w)/8/np.pi/np.pi/Brho/r_w/r_w
    dqy = mu0*I_L*bety*np.cos(2*phi_w)/8/np.pi/np.pi/Brho/r_w/r_w
    return [dqx,dqy]