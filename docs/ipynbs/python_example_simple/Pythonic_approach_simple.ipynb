{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Introduction\n",
    "\n",
    "In this [notebook](https://gitlab.cern.ch/sterbini/lhcmaskdoc/-/blob/master/docs/ipynbs/python_example_simple/Pythonic_approach_simple.ipynb), we are aiming to use python as scripting language for the MAD-X mask. The goal is to bring together the scripting flexibility of python and the optics capability from MAD-X.\n",
    "\n",
    "The rationale is to use [cpymad](https://github.com/hibtc/cpymad) to interface python to MAD-X.\n",
    "\n",
    "Beyond the standard python setup, to run this notebook you need the following packages\n",
    "\n",
    "- [cpymad](https://github.com/hibtc/cpymad)  \n",
    "- [madxp](https://github.com/sterbini/madxp.git)\n",
    "- [fillingpatterns](https://github.com/PyCOMPLETE/FillingPatterns)\n",
    "\n",
    "\n",
    "In addition you need AFS mounted and access to \n",
    "```bash\n",
    "/afs/cern.ch/eng/lhc/optics/\n",
    "/afs/cern.ch/eng/tracking_tools\n",
    "/afs/cern.ch/user/s/sterbini/public/tracking_tools   # to be replaced once consolidated\n",
    "```\n",
    "that you should already have.\n",
    "\n",
    "!!! warning\n",
    "    We assume that the user knows MAD-X and python.\n",
    "    \n",
    "!!! danger\n",
    "    Values chosen are arbitrary!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Importing the packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here you are the \"special\" packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from madxp import luminosity as lumi\n",
    "from cpymad.madx import Madx\n",
    "from madxp import cpymadTool as mt\n",
    "import fillingpatterns as fp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This are standard packages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from collections import OrderedDict\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from matplotlib import pylab as plt\n",
    "from IPython.display import display\n",
    "import time\n",
    "import os\n",
    "import warnings\n",
    "import shutil\n",
    "import urllib.request, json \n",
    "warnings.filterwarnings('always')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To import the filling pattern you can use the approach proposed [here](https://github.com/PyCOMPLETE/FillingPatterns).\n",
    "\n",
    "!!! warning\n",
    "    Please note that the chose pattern is arbitratry."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "with urllib.request.urlopen('https://raw.githubusercontent.com/PyCOMPLETE/FillingPatterns/master/examples/25ns_2744b_2736_2246_2370_240bpi_13inj_800ns_bs200ns_BCMS_5x48b.json') as url:\n",
    "    data = json.loads(url.read().decode())\n",
    "    bb_pattern = fp.FillingPattern(data['beam1'], data['beam2'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mask parameters\n",
    "\n",
    "In the **parameter_dict** we will store the parameters needed for simulation.\n",
    "\n",
    "!!! warning\n",
    "    Remember that MAD-X does not accept \"strings\" as variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %% Definition of the parameters that are not knobs of the beam sequence (no strings please!)\n",
    "parameter_dict={\n",
    "    # =============================================================================\n",
    "    # Beam parameters\n",
    "    # =============================================================================\n",
    "    ## LHC beam 1 (clockwise), LHC beam 2 (clockwise), LHC beam 2 (counterclockwise) \n",
    "    'par_mylhcbeam': 1, \n",
    "    ## beam normalized emittance [m rad]\n",
    "    'par_beam_norm_emit': 2.5e-6,\n",
    "    ## [m]\n",
    "    'par_beam_sigt': 0.075,\n",
    "    ## [-]           \n",
    "    'par_beam_sige': 1.1e-4,\n",
    "    ## [-]                    \n",
    "    'par_beam_npart': 1.16e11, \n",
    "    ## [GeV]            \n",
    "    'par_beam_energy_tot': 7000,\n",
    "    ## [A]          \n",
    "    'par_oct_current': 350,\n",
    "    ## [-]            \n",
    "    'par_chromaticity': 15,\n",
    "    ## [MV]          \n",
    "    'par_vrf_total': 16.,\n",
    "    ## Tunes with fractional part          \n",
    "    'par_qx0': 62.31, 'par_qy0': 60.32,\n",
    "    # =============================================================================\n",
    "    # Beam-Beam configuration \n",
    "    # =============================================================================\n",
    "    ## install the BB elements [0,1]\n",
    "    'par_on_bb_switch': 1,\n",
    "    ## if 1 lumi leveling in ip8 is applied and q/q' match is done with bb off [0,1]\n",
    "    'par_on_collision': 1, \n",
    "    ## bunch separation [ns]               \n",
    "    'par_b_t_dist': 25.,   \n",
    "    ## default value for the number of additionnal parasitic encounters inside D1              \n",
    "    'par_n_inside_D1': 5,                 \n",
    "    ## number of slices for head-on in IR1 [between 0 and 201]\n",
    "    'par_nho_IR1': 11, 'par_nho_IR2': 11, 'par_nho_IR5': 11, 'par_nho_IR8': 11, \n",
    "    ## flag to install the Crab Cavities [0, 1]\n",
    "    'par_install_crabcavities': 0,\n",
    "    # can be negative positive or zero to switch of spectr in lhcb\n",
    "    'par_lhcb_polarity': 1., \n",
    "    # =============================================================================\n",
    "    # Leveling in IP8   \n",
    "    # =============================================================================\n",
    "    # leveled luminosity in IP8 (considered if par_on_collision=1) [Hz/cm2]\n",
    "    'par_lumi_ip8': 2e33,                 \n",
    "    # These variables define the number of Head-On collisions in the 4 IPs\n",
    "    'par_nco_IP1': bb_pattern.n_coll_ATLAS,\n",
    "    'par_nco_IP2': bb_pattern.n_coll_ALICE,\n",
    "    'par_nco_IP5': bb_pattern.n_coll_CMS,\n",
    "    'par_nco_IP8': bb_pattern.n_coll_LHCb,\n",
    "    # =============================================================================\n",
    "    # Errors and corrections \n",
    "    # =============================================================================\n",
    "    # Select seed for errors\n",
    "    'par_myseed': 0,\n",
    "    # Set this flag to correct the errors of D2 in the NLC \n",
    "    # (warning: for now only correcting b3 of D2, still in development)\n",
    "    'par_correct_for_D2': 0,\n",
    "    # Set this flag to correct the errors of MCBXF in the NLC \n",
    "    # (warning: this might be less reproducable in reality, use with care)\n",
    "    'par_correct_for_MCBX': 0,\n",
    "    'par_off_all_errors': 0,\n",
    "    'par_on_errors_LHC': 0,\n",
    "    'par_on_errors_MBH': 0,\n",
    "    'par_on_errors_Q5': 0,\n",
    "    'par_on_errors_Q4': 0,\n",
    "    'par_on_errors_D2': 0,\n",
    "    'par_on_errors_D1': 0,\n",
    "    'par_on_errors_IT': 0,\n",
    "    'par_on_errors_MCBRD': 0,\n",
    "    'par_on_errors_MCBXF': 0,\n",
    "    # =============================================================================\n",
    "    # Additional parameters\n",
    "    # =============================================================================\n",
    "    # parameter for having verbose output [0,1]\n",
    "    'par_verbose': 1,\n",
    "    # definition of the slicefactor used in the makethin\n",
    "    'par_slicefactor': 4,\n",
    "    # number of optics to use\n",
    "    'par_optics_number':30,\n",
    "    # Specify machine version\n",
    "    'ver_lhc_run' : 3, 'ver_hllhc_optics' : 0,\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The make_sequence function\n",
    "\n",
    "The user has to define a \"make_sequence\" function. The function will take the MAD-X handle, the beam number (1, 2 or 4) and, in this case, the slice factor (to make the sequence thin). \n",
    "\n",
    "!!! info\n",
    "    It is important to know that we clearly define an interface that isolate the function from MAD-X.\n",
    "    \n",
    "!!! hint \n",
    "    Take the time to consider the different ```madx.call```.\n",
    "    \n",
    "!!! info\n",
    "    This function refers to links (e.g., ```macro.madx```) that will be defined later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_sequence(mad, mylhcbeam, slicefactor):\n",
    "    '''\n",
    "    User-defined function to make the Run3 optics.\n",
    "    '''\n",
    "    \n",
    "    start_time = time.time()\n",
    "    \n",
    "    mad.input('option, -echo,warn, -info;')\n",
    "    # optics dependent macros\n",
    "    mad.call('macro.madx') \n",
    "    # optics dependent macros\n",
    "    mad.call('optics_indep_macros.madx')\n",
    "\n",
    "    assert mylhcbeam in [1, 2, 4], \"Invalid mylhcbeam (it should be in [1, 2, 4])\"\n",
    "\n",
    "    if mylhcbeam in [1, 2]:\n",
    "        mad.call('optics_runII/2018/lhc_as-built.seq')\n",
    "    else:\n",
    "        mad.call('optics_runII/2018/lhcb4_as-built.seq')\n",
    "\n",
    "    # New IR7 MQW layout and cabling\n",
    "    mad.call('optics_runIII/RunIII_dev/IR7-Run3seqedit.madx')\n",
    "    \n",
    "    # Makethin part\n",
    "    if slicefactor > 0:\n",
    "        mad.input(f'slicefactor={slicefactor};') # the variable in the macro is slicefactor\n",
    "        mad.call('optics_runII/2018/toolkit/myslice.madx')\n",
    "        mad.beam()\n",
    "        for my_sequence in ['lhcb1','lhcb2']:\n",
    "            if my_sequence in list(mad.sequence):\n",
    "                mad.input(f'use, sequence={my_sequence}; makethin, sequence={my_sequence}, style=teapot, makedipedge=false;')\n",
    "    else:\n",
    "        warnings.warn('The sequences are not thin!')\n",
    "\n",
    "    # Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)\n",
    "    for my_sequence in ['lhcb1','lhcb2']:\n",
    "        if my_sequence in list(mad.sequence):\n",
    "            mad.input(f'seqedit, sequence={my_sequence}; flatten; cycle, start=IP3; flatten; endedit;')\n",
    "\n",
    "    my_output_dict = get_status(mad)\n",
    "    elapsed_time = time.time() - start_time\n",
    "    my_output_dict['elapsed_time'] = elapsed_time\n",
    "    return my_output_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The load_optics function\n",
    "\n",
    "This is the (very simple) function to load the the optics file.\n",
    "\n",
    "!!! info\n",
    "    This function refers to links (e.g., ```optics.madx```) that will be defined later.\n",
    "    \n",
    "!!! hint\n",
    "    We prefer to use links more than variables so that, in the folder of the simulation you have (at least some of) the links used from your mask."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def load_optics(mad):\n",
    "    '''\n",
    "    User-defined function load the optics file.\n",
    "    '''\n",
    "    start_time = time.time()\n",
    "    \n",
    "    # nothing very special\n",
    "    mad.call(f'optics.madx')\n",
    "    \n",
    "    my_output_dict = get_status(mad)\n",
    "    elapsed_time = time.time() - start_time\n",
    "    my_output_dict['elapsed_time'] = elapsed_time\n",
    "    return my_output_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Auxiliary functions \n",
    "\n",
    "To improve the analysis and debugging of the mask is useful to have some auxiliary functions.\n",
    "!!! info\n",
    "    We plan to move them to a package.\n",
    "    \n",
    "!!! warning\n",
    "    Please check the files that will be removed by the **clean_folder** function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_status(mad):\n",
    "    '''\n",
    "    Return the status of the variables, sequences, beams and tables of a MAD-X object (mad).\n",
    "    '''\n",
    "    start_time = time.time()\n",
    "    \n",
    "    variables=mt.variables_dict(mad)\n",
    "    \n",
    "    my_output_dict= {'constant_df': variables['constant_df'],\n",
    "            'independent_variable_df': variables['independent_variable_df'],\n",
    "            'dependent_variable_df': variables['dependent_variable_df'],\n",
    "            'sequences_df': mt.sequences_df(mad),\n",
    "            'beams_df': mt.beams_df(mad),\n",
    "            'tables_list': list(mad.table)}\n",
    "    elapsed_time = time.time() - start_time\n",
    "    my_output_dict['elapsed_time'] = elapsed_time\n",
    "    return my_output_dict\n",
    "\n",
    "def run_module(mad, module_name):\n",
    "    '''\n",
    "    Run the module_name in the MAD-X object (mad).\n",
    "    '''\n",
    "    start_time = time.time()\n",
    "    \n",
    "    mad.call(f'modules/{module_name}')\n",
    "    \n",
    "    my_output_dict = get_status(mad)\n",
    "    elapsed_time = time.time() - start_time\n",
    "    my_output_dict['elapsed_time']= elapsed_time\n",
    "    return my_output_dict\n",
    "\n",
    "def read_parameters(mad, parameter_dict):\n",
    "    '''\n",
    "    Assign the parameter_dict to the MAD-X object (mad).\n",
    "    '''\n",
    "    start_time = time.time()\n",
    "    \n",
    "    parameter_dict['par_qx00']=int(parameter_dict['par_qx0'])\n",
    "    parameter_dict['par_qy00']=int(parameter_dict['par_qy0'])\n",
    "    parameter_dict['par_tsplit']=parameter_dict['par_qx00']-parameter_dict['par_qy00']\n",
    "    \n",
    "    assert parameter_dict['par_nco_IP5']==parameter_dict['par_nco_IP1']\n",
    "    assert parameter_dict['par_qx00']-parameter_dict['par_qy00']==parameter_dict['par_tsplit']\n",
    "    assert 'par_mylhcbeam' in parameter_dict\n",
    "    assert 'par_beam_norm_emit' in parameter_dict\n",
    "    assert 'par_optics_number' in parameter_dict, 'Optics file not defined.'\n",
    "\n",
    "    for i in parameter_dict:\n",
    "        if isinstance(parameter_dict[i], (float,int)):\n",
    "            mad.input(f'{i}={parameter_dict[i]};')\n",
    "   \n",
    "    my_output_dict = get_status(mad)\n",
    "    elapsed_time = time.time() - start_time\n",
    "    my_output_dict['elapsed_time']= elapsed_time\n",
    "    return my_output_dict\n",
    "\n",
    "def clean_folder(file_string='fc.* *parquet twiss* log.madx stdout.madx bb_lenses.dat last_twiss.0.gz temp', rm_links=True):\n",
    "    '''\n",
    "    Remove the folder from the MAD-X output.\n",
    "    '''\n",
    "    if rm_links:\n",
    "        os.system('find -type l -delete')\n",
    "    os.system('rm -rf '+ file_string)\n",
    "\n",
    "def check_links(my_path='.'):\n",
    "    '''\n",
    "    Checks the validity of the links.\n",
    "    '''\n",
    "    symlinks = [i for i in os.listdir(my_path)]\n",
    "    for i in symlinks:\n",
    "        if os.path.islink(i):\n",
    "            assert os.path.exists(i), f'Link to {i} is broken.'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Make links\n",
    "\n",
    "!!! warning\n",
    "    Establish the correct links is very important since the links are directly used in the python functions and in the MAD-X modules. \n",
    "    \n",
    "!!! warning\n",
    "    The link to the ```/afs/cern.ch/user/s/sterbini/public/tracking_tools/modules``` will be removed once the folder will be merged with te official repository ```/afs/cern.ch/eng/tracking-tools/modules```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "#%% Make links for setting the enviroments\n",
    "clean_folder()\n",
    "# Main path\n",
    "os.symlink('/afs/cern.ch/eng/tracking-tools', 'tracking_tools')\n",
    "# Mask code folder\n",
    "os.symlink('/afs/cern.ch/user/s/sterbini/public/tracking_tools/modules', 'modules')\n",
    "# Machine folder\n",
    "os.symlink('tracking_tools/machines', 'machines')\n",
    "# Toolkit folder\n",
    "os.symlink('tracking_tools/tools', 'tools')\n",
    "# Beam-beam macros folder\n",
    "os.symlink('tracking_tools/beambeam_macros', 'beambeam_macros')\n",
    "# Errors folder\n",
    "os.symlink('tracking_tools/errors', 'errors')\n",
    "# RunII optics\n",
    "os.symlink('/afs/cern.ch/eng/lhc/optics/runII', 'optics_runII')\n",
    "# RunIII optics\n",
    "os.symlink('/afs/cern.ch/eng/lhc/optics/runIII', 'optics_runIII')\n",
    "# Load optics (magnet strengths)\n",
    "op  = int(parameter_dict['par_optics_number'])\n",
    "os.symlink(f'optics_runIII/RunIII_dev/2022_V1/PROTON/opticsfile.{op}', 'optics.madx')\n",
    "\n",
    "# General macros\n",
    "# optics dependent macros\n",
    "#os.symlink('optics_runII/2018/toolkit/macro.madx', 'macro.madx')\n",
    "os.symlink('/afs/cern.ch/work/s/sterbini/tracking_tools/tools/macro.madx', 'macro.madx')\n",
    "\n",
    "# optics independent macros\n",
    "os.symlink('tools/optics_indep_macros.madx', 'optics_indep_macros.madx')\n",
    "check_links()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Launch MAD-X from python"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "command_log_file='log.madx'\n",
    "stdout_file='stdout.madx'\n",
    "with open(stdout_file, 'w') as myFile:\n",
    "    mad = Madx(stdout=myFile,command_log=command_log_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preliminary optics checks\n",
    "\n",
    "In the following cell we execute the functions\n",
    "\n",
    "    - read_parameters\n",
    "    - make_sequence\n",
    "    - load_optics\n",
    "\n",
    "then we run the module ```module_00_check_optics.madx```.\n",
    "\n",
    "In a similar way we could ran all the remaing module of the mask. Before executing them we will analyze and do sanity checks on the optics.\n",
    "\n",
    "!!! info\n",
    "    While executing the code we build up a work-flow dictionary (```my_workflow_dict```). Indeed, all the fuction return a dictionary with metadata used for debugging or automatically asserting the work-flow. We will do plenty of example in the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start making MAD-X operation\n",
    "my_workflow_dict = OrderedDict()\n",
    "\n",
    "my_workflow_dict['read_parameters'] = read_parameters(mad, parameter_dict)\n",
    "my_workflow_dict['make_sequence'] = make_sequence(mad, parameter_dict['par_mylhcbeam'], parameter_dict['par_slicefactor'])\n",
    "my_workflow_dict['load_optics'] = load_optics(mad)\n",
    "my_workflow_dict['check_optics'] = run_module(mad,'module_00_check_optics.madx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting the crossing angles and separations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# This is the relation from DA studies from Nikos and Stéphane.\n",
    "def from_beta_to_xing_angle_urad(beta_m):\n",
    "    return  0.5*(132.47 + 58.3959 * np.sqrt(beta_m) + 30.0211 * beta_m)/np.sqrt(beta_m)\n",
    "\n",
    "knob_dict={\n",
    "    'on_sep1': 0,  \n",
    "    'on_sep5': 0,         \n",
    "    'on_sep2h': 2,\n",
    "    'on_sep2v': 0,\n",
    "    'on_x2h': 0,\n",
    "    'on_x2v': 200,\n",
    "    'on_sep8h': 0,\n",
    "    'on_sep8v': 1,\n",
    "    'on_x8h': 0,\n",
    "    'on_x8v': 135,\n",
    "    'on_disp': 1,\n",
    "    'on_alice': 7000/parameter_dict['par_beam_energy_tot'],\n",
    "    'on_lhcb': 7000/parameter_dict['par_beam_energy_tot'],\n",
    "    'on_sol_atlas': 7000/parameter_dict['par_beam_energy_tot'],\n",
    "    'on_sol_cms': 7000/parameter_dict['par_beam_energy_tot'],\n",
    "    'on_sol_alice': 7000/parameter_dict['par_beam_energy_tot'],\n",
    "}\n",
    "\n",
    "betx_ip1 = mad.globals['betx_ip1']\n",
    "knob_dict['on_x1'] = from_beta_to_xing_angle_urad(betx_ip1)\n",
    "knob_dict['on_x5'] = from_beta_to_xing_angle_urad(betx_ip1)\n",
    "\n",
    "for i in knob_dict:\n",
    "    mad.input(f'{i} = {knob_dict[i]};')\n",
    "\n",
    "mad.input('on_sep8=on_sep8v;')\n",
    "mad.input('on_sep2=on_sep2h;')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Saving the reference CO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_workflow_dict['save_crossing'] = run_module(mad,'module_01_save_crossing.madx')\n",
    "execution_df=pd.DataFrame(my_workflow_dict).transpose()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### About Luminosity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Luminosity at IP1: 2.1084853766379376e+34 Hz/cm^2\n",
      "Luminosity at IP2: 0.0 Hz/cm^2\n",
      "Luminosity at IP5: 2.1065742028275344e+34 Hz/cm^2\n",
      "Luminosity at IP8: 0.0 Hz/cm^2\n"
     ]
    }
   ],
   "source": [
    "from madxp import luminosity as lumi\n",
    "\n",
    "B1=mad.sequence.lhcb1.beam\n",
    "B2=mad.sequence.lhcb2.beam\n",
    "\n",
    "#check the frequency\n",
    "assert B1.freq0==B2.freq0\n",
    "\n",
    "mad.twiss(sequence='lhcb1'); B1_DF=mt.twiss_df(mad.table.twiss)\n",
    "mad.twiss(sequence='lhcb2'); B2_DF=mt.twiss_df(mad.table.twiss)\n",
    "\n",
    "def check_luminosity(B1,B2,B1_DF,B2_DF):\n",
    "    for myIP in ['IP1','IP2', 'IP5', 'IP8']:\n",
    "        B1_IP=B1_DF.loc[myIP.lower()+':1']\n",
    "        B2_IP=B2_DF.loc[myIP.lower()+':1']\n",
    "        aux=lumi.L(f=B1.freq0*1e6, nb=parameter_dict['par_nco_'+myIP],\n",
    "            N1=B1.npart, N2=B2.npart,\n",
    "            energy_tot1=B1.energy, energy_tot2=B2.energy,\n",
    "            deltap_p0_1=B1.sige, deltap_p0_2=B2.sige,\n",
    "            epsilon_x1=B1.exn, epsilon_x2=B2.exn,\n",
    "            epsilon_y1=B1.eyn, epsilon_y2=B2.eyn, \n",
    "            sigma_z1=B1.sigt, sigma_z2=B2.sigt,\n",
    "            beta_x1=B1_IP.betx, beta_x2=B2_IP.betx,\n",
    "            beta_y1=B1_IP.bety, beta_y2=B2_IP.bety,\n",
    "            alpha_x1=B1_IP.alfx, alpha_x2=B2_IP.alfx,\n",
    "            alpha_y1=B1_IP.alfy, alpha_y2=B2_IP.alfy,\n",
    "            dx_1=B1_IP.dx, dx_2=B2_IP.dx,\n",
    "            dpx_1=B1_IP.dpx, dpx_2=B2_IP.dpx,\n",
    "            dy_1=B1_IP.dy, dy_2=B2_IP.dy,\n",
    "            dpy_1=B1_IP.dpy, dpy_2=B2_IP.dpy,\n",
    "            x_1=B1_IP.x, x_2=B2_IP.x,\n",
    "            px_1=B1_IP.px, px_2=B2_IP.px,\n",
    "            y_1=B1_IP.y, y_2=B2_IP.y,\n",
    "            py_1=B1_IP.py, py_2=B2_IP.py, verbose=False)\n",
    "        print(f'Luminosity at {myIP}: {aux} Hz/cm^2')\n",
    "\n",
    "check_luminosity(B1,B2,B1_DF,B2_DF)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "!!! info\n",
    "    The IP2/IP8 are separated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Luminosity at IP1: 2.1084853766379376e+34 Hz/cm^2\n",
      "Luminosity at IP2: 0.0 Hz/cm^2\n",
      "Luminosity at IP5: 2.1065742028275344e+34 Hz/cm^2\n",
      "Luminosity at IP8: 0.0 Hz/cm^2\n",
      "After B1/2 inversion\n",
      "Luminosity at IP1: 2.1084853766379376e+34 Hz/cm^2\n",
      "Luminosity at IP2: 0.0 Hz/cm^2\n",
      "Luminosity at IP5: 2.1065742028275344e+34 Hz/cm^2\n",
      "Luminosity at IP8: 0.0 Hz/cm^2\n"
     ]
    }
   ],
   "source": [
    "# we are inverting the beams\n",
    "check_luminosity(B1,B2,B1_DF,B2_DF)\n",
    "print('After B1/2 inversion')\n",
    "check_luminosity(B2,B1,B2_DF,B1_DF)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "!!! success\n",
    "    If one invert the B1/2 you get the same luminosity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Leveling in intensity\n",
    "\n",
    "We can \"level\" the beam intensity to the average luminosity in IP1 and IP5. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "==== Offset Levelling ====\n",
      " active_mask: array([0.])\n",
      "        cost: 2.658455991569832e+36\n",
      "         fun: array([2.30584301e+18])\n",
      "        grad: array([8.16213177e+41])\n",
      "         jac: array([[3.5397604e+23]])\n",
      "     message: '`xtol` termination condition is satisfied.'\n",
      "        nfev: 5\n",
      "        njev: 5\n",
      "  optimality: 8.16213177232973e+41\n",
      "      status: 3\n",
      "     success: True\n",
      "           x: array([1.13001999e+11])\n",
      "\n",
      "Luminosity after levelling: 2.0000000000000001e+34 Hz/cm^2\n"
     ]
    }
   ],
   "source": [
    "from scipy.optimize import least_squares\n",
    "\n",
    "print('\\n==== Offset Levelling ====')\n",
    "L_target=2e+34\n",
    "starting_guess=B1.npart\n",
    "\n",
    "def function_to_minimize(N):\n",
    "    B1_IP=B1_DF.loc['ip1:1']\n",
    "    B2_IP=B2_DF.loc['ip1:1']\n",
    "    L_IP1=lumi.L(f=B1.freq0*1e6, nb=parameter_dict['par_nco_IP1'],\n",
    "        N1=N, N2=N,\n",
    "        energy_tot1=B1.energy, energy_tot2=B2.energy,\n",
    "        deltap_p0_1=B1.sige, deltap_p0_2=B2.sige,\n",
    "        epsilon_x1=B1.exn, epsilon_x2=B2.exn,\n",
    "        epsilon_y1=B1.eyn, epsilon_y2=B2.eyn, \n",
    "        sigma_z1=B1.sigt, sigma_z2=B2.sigt,\n",
    "        beta_x1=B1_IP.betx, beta_x2=B2_IP.betx,\n",
    "        beta_y1=B1_IP.bety, beta_y2=B2_IP.bety,\n",
    "        alpha_x1=B1_IP.alfx, alpha_x2=B2_IP.alfx,\n",
    "        alpha_y1=B1_IP.alfy, alpha_y2=B2_IP.alfy,\n",
    "        dx_1=B1_IP.dx, dx_2=B2_IP.dx,\n",
    "        dpx_1=B1_IP.dpx, dpx_2=B2_IP.dpx,\n",
    "        dy_1=B1_IP.dy, dy_2=B2_IP.dy,\n",
    "        dpy_1=B1_IP.dpy, dpy_2=B2_IP.dpy,\n",
    "        x_1=B1_IP.x, x_2=B2_IP.x,\n",
    "        px_1=B1_IP.px, px_2=B2_IP.px,\n",
    "        y_1=B1_IP.y, y_2=B2_IP.y,\n",
    "        py_1=B1_IP.py, py_2=B2_IP.py, verbose=False)\n",
    "    \n",
    "    B1_IP=B1_DF.loc['ip5:1']\n",
    "    B2_IP=B2_DF.loc['ip5:1']\n",
    "    L_IP5=lumi.L(f=B1.freq0*1e6, nb=parameter_dict['par_nco_IP5'],\n",
    "        N1=N, N2=N,\n",
    "        energy_tot1=B1.energy, energy_tot2=B2.energy,\n",
    "        deltap_p0_1=B1.sige, deltap_p0_2=B2.sige,\n",
    "        epsilon_x1=B1.exn, epsilon_x2=B2.exn,\n",
    "        epsilon_y1=B1.eyn, epsilon_y2=B2.eyn, \n",
    "        sigma_z1=B1.sigt, sigma_z2=B2.sigt,\n",
    "        beta_x1=B1_IP.betx, beta_x2=B2_IP.betx,\n",
    "        beta_y1=B1_IP.bety, beta_y2=B2_IP.bety,\n",
    "        alpha_x1=B1_IP.alfx, alpha_x2=B2_IP.alfx,\n",
    "        alpha_y1=B1_IP.alfy, alpha_y2=B2_IP.alfy,\n",
    "        dx_1=B1_IP.dx, dx_2=B2_IP.dx,\n",
    "        dpx_1=B1_IP.dpx, dpx_2=B2_IP.dpx,\n",
    "        dy_1=B1_IP.dy, dy_2=B2_IP.dy,\n",
    "        dpy_1=B1_IP.dpy, dpy_2=B2_IP.dpy,\n",
    "        x_1=B1_IP.x, x_2=B2_IP.x,\n",
    "        px_1=B1_IP.px, px_2=B2_IP.px,\n",
    "        y_1=B1_IP.y, y_2=B2_IP.y,\n",
    "        py_1=B1_IP.py, py_2=B2_IP.py, verbose=False)\n",
    "    \n",
    "    return 0.5*(L_IP1+L_IP5)-L_target\n",
    "\n",
    "aux=least_squares(function_to_minimize, starting_guess)\n",
    "print(aux)\n",
    "print(f\"\\nLuminosity after levelling: {function_to_minimize(aux['x'][0])+L_target} Hz/cm^2\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we move from python to MAD-X and we recompute the luminosity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Luminosity at IP1: 2.000906831219997e+34 Hz/cm^2\n",
      "Luminosity at IP2: 0.0 Hz/cm^2\n",
      "Luminosity at IP5: 1.9990931687800033e+34 Hz/cm^2\n",
      "Luminosity at IP8: 0.0 Hz/cm^2\n"
     ]
    }
   ],
   "source": [
    "mad.sequence.lhcb1.beam.npart=aux.x[0]\n",
    "mad.sequence.lhcb2.beam.npart=aux.x[0]\n",
    "B1=mad.sequence.lhcb1.beam\n",
    "B2=mad.sequence.lhcb2.beam\n",
    "check_luminosity(B1,B2,B1_DF,B2_DF)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.0"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(2.0011449627161415+1.9988550372838583)/2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "!!! success\n",
    "    The \"intensity leveling\" is working."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Offset in IP8\n",
    "We can now level the IP8 by separation.\n",
    "\n",
    "!!! info\n",
    "    In the original mask before leveling the on_disp is forced to 0 before the leveling (and re-established after the leveling macros). We are NOT forcing it to 0 in the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "==== Offset Levelling ====\n",
      " active_mask: array([0])\n",
      "        cost: 2.9074086151568487e+41\n",
      "         fun: array([7.62549489e+20])\n",
      "        grad: array([-2.21281711e+58])\n",
      "         jac: array([[-2.90186688e+37]])\n",
      "     message: '`xtol` termination condition is satisfied.'\n",
      "        nfev: 9\n",
      "        njev: 9\n",
      "  optimality: 5.688322531645036e+54\n",
      "      status: 3\n",
      "     success: True\n",
      "           x: array([4.29375201e-05])\n",
      "\n",
      "Luminosity after levelling: 2.0000000000076256e+32 Hz/cm^2\n"
     ]
    }
   ],
   "source": [
    "print('\\n==== Offset Levelling ====')\n",
    "L_target=2e+32\n",
    "starting_guess=1e-5\n",
    "\n",
    "B1_IP=B1_DF.loc['ip8:1']\n",
    "B2_IP=B2_DF.loc['ip8:1']\n",
    "\n",
    "def function_to_minimize(delta):\n",
    "    aux=lumi.L(f=B1.freq0*1e6, nb=parameter_dict['par_nco_IP8'],\n",
    "        N1=B1.npart, N2=B2.npart,\n",
    "        energy_tot1=B1.energy, energy_tot2=B2.energy,\n",
    "        deltap_p0_1=B1.sige, deltap_p0_2=B2.sige,\n",
    "        epsilon_x1=B1.exn, epsilon_x2=B2.exn,\n",
    "        epsilon_y1=B1.eyn, epsilon_y2=B2.eyn, \n",
    "        sigma_z1=B1.sigt, sigma_z2=B2.sigt,\n",
    "        beta_x1=B1_IP.betx, beta_x2=B2_IP.betx,\n",
    "        beta_y1=B1_IP.bety, beta_y2=B2_IP.bety,\n",
    "        alpha_x1=B1_IP.alfx, alpha_x2=B2_IP.alfx,\n",
    "        alpha_y1=B1_IP.alfy, alpha_y2=B2_IP.alfy,\n",
    "        dx_1=B1_IP.dx, dx_2=B2_IP.dx,\n",
    "        dpx_1=B1_IP.dpx, dpx_2=B2_IP.dpx,\n",
    "        dy_1=B1_IP.dy, dy_2=B2_IP.dy,\n",
    "        dpy_1=B1_IP.dpy, dpy_2=B2_IP.dpy,\n",
    "        x_1=B1_IP.x, x_2=B2_IP.x,\n",
    "        px_1=B1_IP.px, px_2=B2_IP.px,\n",
    "        y_1=delta, y_2=-delta,\n",
    "        py_1=B1_IP.py, py_2=B2_IP.py, verbose=False)\n",
    "    \n",
    "    return aux-L_target\n",
    "\n",
    "aux=least_squares(function_to_minimize, starting_guess, bounds=(0, 3e-4))\n",
    "print(aux)\n",
    "print(f\"\\nLuminosity after levelling: {function_to_minimize(aux['x'][0])+L_target} Hz/cm^2\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Setting the python result in MAD-X"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "BEFORE: on_sep8v=1.0\n",
      "AFTER: on_sep8v=0.04293752005282778\n"
     ]
    }
   ],
   "source": [
    "print(f\"BEFORE: on_sep8v={mad.globals['on_sep8v']}\")\n",
    "mad.globals['on_sep8v']=aux.x[0]*1e3\n",
    "print(f\"AFTER: on_sep8v={mad.globals['on_sep8v']}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sanity check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Luminosity at IP1: 2.00108737607358e+34 Hz/cm^2\n",
      "Luminosity at IP2: 0.0 Hz/cm^2\n",
      "Luminosity at IP5: 1.9988077575957086e+34 Hz/cm^2\n",
      "Luminosity at IP8: 1.9751712055946312e+32 Hz/cm^2\n"
     ]
    }
   ],
   "source": [
    "B1=mad.sequence.lhcb1.beam\n",
    "B2=mad.sequence.lhcb2.beam\n",
    "\n",
    "#check the frequency\n",
    "assert B1.freq0==B2.freq0\n",
    "\n",
    "mad.twiss(sequence='lhcb1'); B1_DF=mt.twiss_df(mad.table.twiss)\n",
    "mad.twiss(sequence='lhcb2'); B2_DF=mt.twiss_df(mad.table.twiss)\n",
    "check_luminosity(B1,B2,B1_DF,B2_DF)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4.308203834994383e-05\n",
      "-4.295580603092084e-05\n"
     ]
    }
   ],
   "source": [
    "B1_IP=B1_DF.loc['ip8:1']\n",
    "B2_IP=B2_DF.loc['ip8:1']\n",
    "print(B1_IP['y'])\n",
    "print(B2_IP['y'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "!!! info\n",
    "    The finite precision of the knob can impact on the final luminosity (and, very marginally, the second order effect on dispersion, optics...) that we get."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
