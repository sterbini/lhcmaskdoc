# **Mask files for LHC and HL-LHC**

**Contributors:** R. De Maria, S. Fartoukh, M. Giovannozzi, G. Iadarola, Y. Papaphilippou, D. Pellegrini, G. Sterbini, F. Van Der Veken  

---

The mask file is script that uses [MAD-X](https://mad.web.cern.ch/mad/){target=_blank} to prepare the LHC model for tracking simulations, to be performed
for example with [sixdesk](https://github.com/SixTrack/SixDesk){target=_blank}/[sixtrack](http://sixtrack.web.cern.ch/SixTrack/){target=_blank}.

The user can choose between two approaches, both documented in this website:

 * [mask](mask.md): a pure MAD-X approach
 * [pymask](pymask.md): a python approach that uses [cpymad](https://github.com/hibtc/cpymad){target=_blank} as an interface to MAD-X.

Certain advanced features as the configuration of Beam 4 (counter-clockwise) with beam-beam or advanced luminosity leveling are available only in pymask.

The tools are hosted in a centralized repository, available:

 - on AFS at ```/afs/cern.ch/eng/tracking-tools/modules```
 
 - on github at [https://github.com/lhcopt/lhcmask/](https://github.com/lhcopt/lhcmask/){target=_blank}