!!! warning
    This section is incomplete


The module use the following variables and macros (the list is still not complete!).

Please verify that your optics respects the same naming convention:

## Optics related:

**Variables:**
```
on_x1
on_sep1
on_x5
on_sep5
on_x2
on_sep2
on_x8
on_sep8
on_a1
on_o1      
on_a5
on_o5
on_a2
on_o2
on_a8
on_o8
on_crab1
on_crab5  

on_disp
on_qpp

on_alice
on_lhcb
on_sol_atlas
on_sol_cms
on_sol_alice

LAGRF400.B1
LAGRF400.B2
VRF400


```

**Macros:**
```
twiss_opt
check_ip
print_crossing_knob
crossing_save
crossing_disable
crossing_enable
crossing_restore
```

**Scripts**:

```
tools/BetaBeating.madx
tools/FineCouplingCorrectionSimplex.madx
tools/rematchCOIP.madx
tools/rematchCOarc.madx
tools/get_optics_params.madx
tools/BetaBeating.madx

errors/corr_value_limit.madx
errors/corr_limit.madx

# If crab cavities are enabled
tools/enable_crabcavities.madx

```



## Beam-beam related

**Macros**
```
DEFINE_BB_PARAM
LEVEL_PARALLEL_OFFSET_FOR
LEVEL_PARALLEL_OFFSET_FOR
INSTALL_BB_MARK
CALCULATE_BB_LENS
INSTALL_BB_LENS
PRINT_BB_LENSES
REMOVE_BB_MARKER
INSTALL_BB_MARK
INSTALL_BB_MARK
SIXTRACK_INPUT_BB_LENSES
```

**Variables**
```
on_bb_charge
```



## Error related


To be completed...



