---

This page describes the structure and the usage of the LHC mask files. The contents are organized in the following categories:

 * [Introduction](#introduction)
 * [Running a simple example](#running-a-simple-example)
 * [Structure of the main configuration file](mainmask.md)
 * [Interfaces to repositories and tools](interfaces.md)
 * [Description of the code modules](modules.md)
 * [Handling of masked parameters](unmask.md)
 
---


## **Introduction**

The mask file is a [MAD-X](https://mad.web.cern.ch/mad/){target=_blank} text input file that prepares the LHC model used for tracking simulations,
for example with [sixdesk](https://github.com/SixTrack/SixDesk)/[sixtrack](http://sixtrack.web.cern.ch/SixTrack/){target=_blank}.
In the past, the mask was a monolithic long script that interleaved settings with MAD-X code, which was very difficult to use, develop and maintain. 
The new mask file described here overcomes some of these limitations providing a modular version of the mask, 
which separates settings from code. 

The settings are provided through a configuration main file (in the following called ```main.mask```), which is edited by the user to configure the simulation. 
The structure of the ```main.mask``` is described [here](mainmask.md) together with the required external resources (sequences, optics, beam-beam macros, errors).

For most use-cases the user needs to edit only the first part of the ```main.mask```, where most of the settings and parameters are defined. The second part of the mask is calling the different modules.
The functionalities of the modules are described [here](modules.md).

The modules are hosted in a centralized repository, available:

 - on AFS at ```/afs/cern.ch/eng/tracking-tools/modules```
 
 - on github at https://github.com/lhcopt/lhcmask/

The modules are, as much as possible, independent on the machine optics or configuration. 

!!! info
    The modules are maintained by the responsible teams and should not be modified by the users, e.g., in local copies. This in order to prevent uncontrolled duplication of the code. Clearly all users are invited to contribute submitting issues, proposing fixes, asking questions via the [repository](https://github.com/lhcopt/lhcmask/){target=_blank}. 


## **Running a simple example**

!!! important
    To run this example you need a machine having access to AFS, able to run the executables of the mask (in the repositories there are pre-compiled executables only for the UNIX-like platforms) and with python. In practice, it will work from LXPLUS (suggested solution for this simple example) or if you have an UBUNTU machine. It is not expected to work on WINDOWS or MAC/OSX.

To execute an example please create a test folder 

```bash
mkdir test_folder 
```
Get an example main configuration script:
```bash
cd test_folder
cp /afs/cern.ch/eng/tracking-tools/modules/examples/hl_lhc_collision/main.mask .
cp /afs/cern.ch/eng/tracking-tools/modules/examples/hl_lhc_collision/parameters_for_unmask.txt .
```

In the ```main.mask``` you will have all the parameters. For historical reason (driven by the the SixDesk workflow), there are two types of parameters:

- regular parameters (set directly in the ```main.mask```)
- masked parameters (present just as placeholder in the ```main.mask```, e.g., ```%OCT%```). These placeholders have to be unmasked if we want to execute the ```main.mask``` in MAD-X. The values for the unmasking is stored in our case in ```parameters_for_unmask.txt```.

You can change the regular parameters to get your machine configuration by editing the ```main.mask``` and set the masked parameters by editing ```parameters_for_unmask.txt```. Please refers to [Handling of masked parameters](unmask.md) for more details.

You can run the simulation by first unmasking the ```main.mask```. This is done by a simple command
```bash
python /afs/cern.ch/eng/tracking-tools/modules/unmask.py main.mask parameters_for_unmask.txt
```
 
and, finally, you can execute it with MAD-X:

!!! info
    We suggest to use the last version of MAD-X available on /afs/cern.ch/user/m/mad/bin/madx.

```bash
madx main.mask.unmasked
```
That's it. To get familiar with the structure of the ```main.mask``` proceed to [Structure of the main configuration file](mainmask.md).
